fetch('https://swapi.dev/api/films/')
    .then(response => response.json())
    .then(({results}) => {
        results.forEach(
            ({title, episode_id, opening_crawl,characters}) => {
                const ulTitle = document.createElement('ul');
                ulTitle.innerHTML = `Film ------  <strong>${title}</strong>`;
                document.body.appendChild(ulTitle);
                const liEpisode = document.createElement('li');
                liEpisode.innerHTML = `Episode --   <strong>${episode_id}</strong>`;
                ulTitle.appendChild(liEpisode);
                const liOpening = document.createElement('li');
                liOpening.innerHTML = `<em>${opening_crawl}:</em>`;
                ulTitle.appendChild(liOpening);

                Promise.all(characters.map(urlsArray =>
                    fetch(urlsArray)
                        .then(response => response.json())
                )).then((charactersList) => {
                    const ulCharacters = document.createElement('ul');
                    ulCharacters.innerHTML = `<strong>Действующие персонажи:</strong>`;
                    ulTitle.appendChild(ulCharacters);

                    charactersList.forEach(({name}) => {
                        const liCharacters = document.createElement('li');
                        liCharacters.innerHTML = `<em>${name}</em>`;
                        ulCharacters.appendChild(liCharacters);
                    });
                });


            })
    });
//   ---PROMISE ALL---   ---PROMISE ALL---   ---PROMISE ALL---   ---PROMISE ALL---   //

// fetch('https://swapi.dev/api/films/')
//     .then(response => response.json())
//     .then(({results}) => {
//         results.forEach(({characters}) => {
//             Promise.all(characters.map(urlsArray =>
//                 fetch(urlsArray)
//                     .then(response => response.json())
//             )).then((charactersList) => {
//                     const cUl = document.createElement('ul');
//                     cUl.innerHTML = `<strong>В фильме снимались:</strong>`;
//                     document.body.appendChild(cUl);
//
//                     charactersList.forEach(({name}) => {
//                         const cLi = document.createElement('li');
//                         cLi.innerHTML = name;
//                         cUl.appendChild(cLi);
//                 });
//             });
//         });
//     });

